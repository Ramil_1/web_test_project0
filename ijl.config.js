const pkg = require('./package')

module.exports = {
    apiPath: "stubs/api",
    webpackConfig: {
        "output": {
            "publicPath": `/static/web_test_project0/${pkg.version}/`
        }
    },
}
